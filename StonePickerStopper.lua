-- sperrgebiet 2022
-- Unlike others I see the meaning in mods that others can learn, expand and improve them. So feel free to use this in your own mods, 
-- add stuff to it, improve it. Your own creativity is the limit ;) If you want to mention me in the credits fine. If not, I'll live happily anyway :P
-- Yeah, I know. I should do a better job in document my code... Next time, I promise... 
--
-- Stops a stone picker once it's full


StonePickerStopper = {}
StonePickerStopper.eventName = {}
StonePickerStopper.ModName = g_currentModName
StonePickerStopper.ModDirectory = g_currentModDirectory
StonePickerStopper.Version = "1.0.0.0"

StonePickerStopper.debug = fileExists(StonePickerStopper.ModDirectory ..'debug')

--print(string.format('StonePickerStopper v%s - DebugMode %s)', StonePickerStopper.Version, tostring(StonePickerStopper.debug)))

addModEventListener(StonePickerStopper)

function StonePickerStopper:dp(val, fun, msg) -- debug mode, write to log
	if not StonePickerStopper.debug then
		return;
	end

	if msg == nil then
		msg = ' ';
	else
		msg = string.format(' msg = [%s] ', tostring(msg));
	end

	local pre = 'StonePickerStopper DEBUG:';

	if type(val) == 'table' then
		--if #val > 0 then
			print(string.format('%s BEGIN Printing table data: (%s)%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
			DebugUtil.printTableRecursively(val, '.', 0, 3);
			print(string.format('%s END Printing table data: (%s)%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
		--else
		--	print(string.format('%s Table is empty: (%s)%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
		--end
	else
		print(string.format('%s [%s]%s(function = [%s()])', pre, tostring(val), msg, tostring(fun)));
	end
end

function StonePickerStopper.prerequisitesPresent(specializations)
    return true
end


function StonePickerStopper.registerEventListeners(vehicleType)
	SpecializationUtil.registerEventListener(vehicleType, "onDraw", StonePickerStopper)	
	SpecializationUtil.registerEventListener(vehicleType, "onFillUnitFillLevelChanged", StonePickerStopper)	
end

function StonePickerStopper:onDraw(isActiveForInput, isActiveForInputIgnoreSelection, isSelected)

	if self:getIsTurnedOn() and (self:getFillUnitFillLevelPercentage(self.spec_stonePicker.fillUnitIndex) >= 0.95) then
		StonePickerStopper:showBlinkingWarning(g_i18n.modEnvironments[StonePickerStopper.ModName].texts.warning)
	end

end

function StonePickerStopper:onFillUnitFillLevelChanged(fillUnitIndex, fillLevelDelta, fillType, toolType, fillPositionData, appliedDelta)
	local spec = self.spec_stonePicker

	if self.isServer and spec.fillUnitIndex == fillUnitIndex then
		local freeCapacity = self:getFillUnitFreeCapacity(spec.fillUnitIndex)

		if freeCapacity <= 0 and self:getIsTurnedOn() then
			self:setIsTurnedOn(false, false)
			local tractor = self:getAttacherVehicle()
			tractor:stopVehicle()
			if StonePickerStopper:isHired(tractor) then
				tractor:stopCurrentAIJob()
				tractor:startMotor()
			end
			local text = string.format("%s %s %s", g_i18n.modEnvironments[StonePickerStopper.ModName].texts.picker_full1, tractor:getFullName(), g_i18n.modEnvironments[StonePickerStopper.ModName].texts.picker_full2)
			StonePickerStopper:sendNotification(text)
		end
	end
end


function StonePickerStopper:isHired(obj)
	if obj.spec_aiJobVehicle ~= nil then
		if obj.spec_aiJobVehicle.job ~= nil then
			return true
		end
	end
end

function StonePickerStopper:showBlinkingWarning(text)
	g_currentMission:showBlinkingWarning(text, 2000);
end

function StonePickerStopper:sendNotification(text, noEventSend)
	g_currentMission.hud:addSideNotification(FSBaseMission.INGAME_NOTIFICATION_INFO, text)
	StonePickerStopperNotificationEvent.sendEvent(text, noEventSend)
end

StonePickerStopperNotificationEvent = {}
local StonePickerStopperNotificationEvent_mt = Class(StonePickerStopperNotificationEvent, Event)

InitEventClass(StonePickerStopperNotificationEvent, "StonePickerStopperNotificationEvent")

function StonePickerStopperNotificationEvent.emptyNew()
    local self = Event.new(StonePickerStopperNotificationEvent_mt)
	self.className = "StonePickerStopperNotificationEvent"
    return self
end

function StonePickerStopperNotificationEvent.new(text)
    local self = StonePickerStopperNotificationEvent:emptyNew()
    self.text = text
    return self
end

function StonePickerStopperNotificationEvent:writeStream(streamId, connection)
	streamWriteString(streamId, self.text)
end

function StonePickerStopperNotificationEvent:readStream(streamId, connection)
	self.text = streamReadString(streamId, connection)

	self:run(connection)
end

function StonePickerStopperNotificationEvent:run(connection)
	StonePickerStopper:sendNotification(self.text, true)
end

function StonePickerStopperNotificationEvent.sendEvent(text, noEventSend)
	if noEventSend == nil or noEventSend == false then
		if g_server ~= nil then
			g_server:broadcastEvent(StonePickerStopperNotificationEvent.new(text))
		else
			g_client:getServerConnection():sendEvent(StonePickerStopperNotificationEvent.new(text))
		end
	end
end
